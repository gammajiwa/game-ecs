using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class DropSpawnerAuthoring : MonoBehaviour
{
    public GameObject DropPrefabs;
}

public class DropSpawnerBake : Baker<DropSpawnerAuthoring>
{
    public override void Bake(DropSpawnerAuthoring authoring)
    {
        AddComponent(new DropSpawnerComponent
        {
            DropPrefabs = GetEntity(authoring.DropPrefabs)
        });
    }
}
