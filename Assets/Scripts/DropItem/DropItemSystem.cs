using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Burst;
using UnityEngine;

[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
[BurstCompile]
public partial struct DropItemSystem : ISystem
{
    [BurstCompile]
    public void OnUpdate(ref SystemState state)
    {
        var ecb = SystemAPI.GetSingleton<EndFixedStepSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(state.WorldUnmanaged);

        foreach (var (item, position) in SystemAPI.Query<RefRW<DropItemComponent>, RefRW<LocalTransform>>())
        {
            foreach (var player in SystemAPI.Query<RefRW<PlayerData>>())
            {
                if (math.distance(position.ValueRW.Position, player.ValueRW.location) < item.ValueRW.range)
                {
                    item.ValueRW.location = player.ValueRW.location;
                    float3 direction = math.normalize(item.ValueRW.location - position.ValueRW.Position);
                    position.ValueRW.Position += direction * SystemAPI.Time.DeltaTime * item.ValueRO.Speed;
                    if (math.distance(position.ValueRW.Position, player.ValueRW.location) < .5f)
                    {
                        ecb.DestroyEntity(item.ValueRW.entity);
                        player.ValueRW.exp += item.ValueRW.expAmount;
                    }
                }
            }
        }
    }
}

