using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using Unity.Entities;

public class DropItemAuthoring : MonoBehaviour
{
    public float3 location;
    public float range;
    public float Speed;
    public int expAmount;
    public Entity entity;
}
public class DropItemBake : Baker<DropItemAuthoring>
{
    public override void Bake(DropItemAuthoring authoring)
    {
        AddComponent(new DropItemComponent
        {
            location = authoring.location,
            range = authoring.range,
            Speed = authoring.Speed,
            expAmount = authoring.expAmount,
            entity = authoring.entity
        });
    }
}