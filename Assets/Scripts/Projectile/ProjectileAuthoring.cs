using Unity.Burst;
using Unity.Entities;
using UnityEngine;
using Unity.Mathematics;
public class ProjectileAuthoring : MonoBehaviour
{
    public Entity Value;
    public float3 Position;
    public float Speed;

}
public class ProjectileBaker : Baker<ProjectileAuthoring>
{
    public override void Bake(ProjectileAuthoring authoring)
    {
        AddComponent(new ProjectileData
        {
            Value = authoring.Value,
            Position = authoring.Position,
            Speed = authoring.Speed
        });
    }
}
