using Unity.Burst;
using Unity.Entities;
using Unity.Physics;
using Unity.Physics.Authoring;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;
using Unity.Mathematics;

[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
[UpdateAfter(typeof(PhysicsSystemGroup))]
[BurstCompile]
public partial struct ProjectileSystem : ISystem
{
    ComponentLookup<LocalToWorld> positionLookUp;

    [BurstCompile]
    public void OnCreate(ref SystemState state)
    {
        positionLookUp = SystemAPI.GetComponentLookup<LocalToWorld>(true);
    }

    [BurstCompile]
    public void OnUpdate(ref SystemState state)
    {
        var ecbBOS = SystemAPI.GetSingleton<EndFixedStepSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(state.WorldUnmanaged);
        PhysicsWorldSingleton physicsWorldSingleton = SystemAPI.GetSingleton<PhysicsWorldSingleton>();
        //shoot
        foreach (var (towerData, transform) in SystemAPI.Query<RefRW<TowerData>, RefRW<LocalTransform>>())
        {
            towerData.ValueRW.TimeToNextSpawn -= SystemAPI.Time.DeltaTime;
            if (towerData.ValueRO.TimeToNextSpawn < 0)
            {
                ClosestHitCollector<DistanceHit> closestHitCollector = new ClosestHitCollector<DistanceHit>(towerData.ValueRW.Range);
                if (physicsWorldSingleton.OverlapSphereCustom(transform.ValueRW.Position, towerData.ValueRW.Range, ref closestHitCollector, towerData.ValueRW.Filter))
                {
                    towerData.ValueRW.TimeToNextSpawn = towerData.ValueRW.Timer;
                    Entity e = ecbBOS.Instantiate(towerData.ValueRO.Prefab);
                    ecbBOS.AddComponent(e, new LocalTransform()
                    {
                        Position = transform.ValueRW.Position,
                        Scale = .5f
                    });
                    ecbBOS.AddComponent(e, new ProjectileData()
                    {
                        Value = closestHitCollector.ClosestHit.Entity,
                        Position = closestHitCollector.ClosestHit.Position,
                        Speed = 15f
                    });
                    // Debug.Log("ERROR");
                }
            }
        }
        //find near enemies
        positionLookUp.Update(ref state);
        foreach (var (target, transform, entity) in SystemAPI.Query<RefRW<ProjectileData>, RefRW<LocalTransform>>().WithEntityAccess())
        {

            if (positionLookUp.HasComponent(target.ValueRW.Value))
            {
                float3 direction = math.normalize(positionLookUp[target.ValueRW.Value].Position - transform.ValueRW.Position);
                transform.ValueRW.Position += direction * SystemAPI.Time.DeltaTime * target.ValueRO.Speed;
                if (math.distance(transform.ValueRW.Position, positionLookUp[target.ValueRW.Value].Position) < .2f)
                {
                    // var dropSpawnerSystem = SystemAPI.GetBuffer<>
                    ecbBOS.DestroyEntity(target.ValueRW.Value);
                    DropItem(ref state, positionLookUp[target.ValueRW.Value].Position);
                    // ecbBOS.DestroyEntity(entity);
                }
            }
            else
            {
                // If the target is not present, destroy the entity
                ecbBOS.DestroyEntity(entity);
            }
        }
    }

    public void DropItem(ref SystemState state, float3 position)
    {
        var ecb = SystemAPI.GetSingleton<EndFixedStepSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(state.WorldUnmanaged);

        RefRW<DropSpawnerComponent> Drop = SystemAPI.GetSingletonRW<DropSpawnerComponent>();
        Entity item = ecb.Instantiate(Drop.ValueRW.DropPrefabs);
        ecb.AddComponent(item, new LocalTransform
        {
            Position = position,
            Scale = 1f
        });
        ecb.SetComponent(item, new DropItemComponent
        {
            range = 1.5f,
            Speed = 20,
            expAmount = 1,
            entity = item
        });
    }
}
