using Unity.Entities;
using Unity.Transforms;
using TMPro;
using UnityEngine;

public class UiManager : MonoBehaviour
{
    public static UiManager instance;
    public TextMeshProUGUI textEnemyCounter;
    EntityManager entityManager;
    EntityQuery enemyEntityQuery;
    public float rateSpawn = 0.8f;
    public bool destroyAll;
    public bool isStop = false;
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        enemyEntityQuery = entityManager.CreateEntityQuery(typeof(EnemyTag));
    }

    void Update()
    {
        if (Time.frameCount % 30 == 0)
        {
            UpdateEnemyCounter();
        }
    }

    void UpdateEnemyCounter()
    {
        textEnemyCounter.text = "Enemy Count = " + enemyEntityQuery.CalculateEntityCount().ToString();
    }

    public void SetRate(float value)
    {
        rateSpawn = value;
        isStop = false;
    }

    public void SetDes()
    {
        destroyAll = true;
        isStop = true;
    }

}
