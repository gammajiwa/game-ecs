using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
public class XmarkSpawnerAuthoring : MonoBehaviour
{
    public Entity entity;
    public float timer;
    public float3 location;
}

public class XmarkBake : Baker<XmarkSpawnerAuthoring>
{
    public override void Bake(XmarkSpawnerAuthoring authoring)
    {
        AddComponent(new XmarkSpawnerComponent
        {
            Xmark = authoring.entity,
            timer = authoring.timer,
            location = authoring.location
        });
    }
}
