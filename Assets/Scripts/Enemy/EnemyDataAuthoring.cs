using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
public class EnemyDataAuthoring : MonoBehaviour
{
    public float3 location;
    public Entity entity;
    public CollisionFilter collisionFilter;
    public float speed;
    public float timerInit;
}

public class EnemyDataBake : Baker<EnemyDataAuthoring>
{
    public override void Bake(EnemyDataAuthoring authoring)
    {
        AddComponent(new EnemyData
        {
            location = authoring.location,
            entity = authoring.entity,
            speed = authoring.speed,
            timerInit = authoring.timerInit
        });
    }
}


