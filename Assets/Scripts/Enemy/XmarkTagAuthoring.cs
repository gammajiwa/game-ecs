using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;


public class XmarkTagAuthoring : MonoBehaviour
{ }


public class XmarkTagBake : Baker<XmarkTagAuthoring>
{
    public override void Bake(XmarkTagAuthoring authoring)
    {
        AddComponent(new XmarkTag());
    }
}
