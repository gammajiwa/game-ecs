using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Collections;
using UnityEngine;

public partial class EnemySpawnerSystem : SystemBase
{
    private float rateSpawn = 0;
    protected override void OnCreate()
    {
        RequireForUpdate<RandomComponent>();
        RequireForUpdate<EnemySpawnerComponent>();
    }
    protected override void OnUpdate()
    {
        var ecb = SystemAPI.GetSingleton<EndFixedStepSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
        RefRW<RandomComponent> randomComponent = SystemAPI.GetSingletonRW<RandomComponent>();
        EntityQuery enemyEntityQuery = EntityManager.CreateEntityQuery(typeof(EnemyTag));
        EntityQuery XmatkQuery = EntityManager.CreateEntityQuery(typeof(XmarkTag));
        EnemySpawnerComponent enemySpawnerComponent = SystemAPI.GetSingleton<EnemySpawnerComponent>();

        // int amount = 40;
        rateSpawn -= 1 * SystemAPI.Time.DeltaTime;
        if (rateSpawn <= 0 && !UiManager.instance.isStop)
        {
            // if (XmatkQuery.CalculateEntityCount() < amount && enemyEntityQuery.CalculateEntityCount() < amount)
            Entity xmark = EntityManager.Instantiate(enemySpawnerComponent.XmarkPrefabs);
            float3 location = new float3(GetRandomePos(randomComponent));
            EntityManager.AddComponentData(xmark, new LocalTransform
            {
                Position = location,
                Scale = .1f
            });
            EntityManager.AddComponentData(xmark, new XmarkSpawnerComponent
            {
                Xmark = xmark,
                timer = 2,
                location = location
            });
            rateSpawn = UiManager.instance.rateSpawn;
            Debug.Log(rateSpawn);
        }

        foreach (var Xmark in SystemAPI.Query<RefRW<XmarkSpawnerComponent>>())
        {
            Xmark.ValueRW.timer -= SystemAPI.Time.DeltaTime;
            if (Xmark.ValueRW.timer <= 0)
            {
                Entity enemyEntity = ecb.Instantiate(enemySpawnerComponent.EnemyPrefabs);
                ecb.AddComponent(enemyEntity, new EnemyData
                {
                    entity = enemyEntity,
                    speed = .3f,
                    timerInit = 1f
                });
                ecb.AddComponent(enemyEntity, new LocalTransform
                {
                    Position = new float3(Xmark.ValueRW.location.x, .5f, Xmark.ValueRW.location.z),
                    Scale = .5f
                });
                ecb.DestroyEntity(Xmark.ValueRW.Xmark);
            }
        }
    }

    private float3 GetRandomePos(RefRW<RandomComponent> randomComponent)
    {
        return new float3(
         randomComponent.ValueRW.random.NextFloat(-10f, 10f),
            0.1f,
         randomComponent.ValueRW.random.NextFloat(-6f, 6f)
        );
    }
}
