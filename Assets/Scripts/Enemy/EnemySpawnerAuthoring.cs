using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class EnemySpawnerAuthoring : MonoBehaviour
{
    public GameObject EnemyPrefabs;
    public GameObject ExmarkPrefabs;
}

public class EnemySpawnerBake : Baker<EnemySpawnerAuthoring>
{
    public override void Bake(EnemySpawnerAuthoring authoring)
    {
        AddComponent(new EnemySpawnerComponent
        {
            EnemyPrefabs = GetEntity(authoring.EnemyPrefabs),
            XmarkPrefabs = GetEntity(authoring.ExmarkPrefabs),
        });
    }
}
