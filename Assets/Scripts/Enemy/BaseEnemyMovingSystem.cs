using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public partial class BaseEnemyMovingSystem : SystemBase
{
    // private GameObject player;

    protected override void OnStartRunning()
    {
        // player = GameObject.FindGameObjectWithTag("Player");
    }
    protected override void OnUpdate()
    {
        var ecb = SystemAPI.GetSingleton<EndFixedStepSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
        foreach (var (localTransform, enemyData)
            in SystemAPI.Query<RefRW<LocalTransform>, RefRW<EnemyData>>())
        {
            // var test = SystemAPI.GetComponentLookup<LocalTransform>();
            // var op = test[enemyData.ValueRW.entity].Position;
            enemyData.ValueRW.timerInit -= SystemAPI.Time.DeltaTime;
            foreach (var item in SystemAPI.Query<RefRW<PlayerData>>())
            {
                if (enemyData.ValueRW.timerInit < 0)
                {
                    enemyData.ValueRW.location = item.ValueRW.location;
                    float3 direction = math.normalize(enemyData.ValueRW.location - localTransform.ValueRW.Position);
                    localTransform.ValueRW.Position += direction * SystemAPI.Time.DeltaTime * enemyData.ValueRO.speed;
                }
                // if (math.distance(localTransform.ValueRW.Position, enemyData.ValueRW.location) > 1f)
                // {
                // }
                // else
                // {
                //     // ecb.DestroyEntity(enemyData.ValueRW.entity);

                // }
            }
        }
    }


}