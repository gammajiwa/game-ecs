using UnityEngine;
using Unity.Entities;

public partial class DestroyAllSystem : SystemBase
{

    protected override void OnUpdate()
    {
        bool destroyAllFlag = UiManager.instance.destroyAll;
        var ecb = SystemAPI.GetSingleton<EndFixedStepSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);

        if (destroyAllFlag)
        {
            Entities
                .ForEach((Entity entity, in EnemyTag tag) =>
                {
                    ecb.DestroyEntity(entity);
                })
                .Schedule();
            Entities
            .ForEach((Entity entity, in XmarkTag tag) =>
            {
                ecb.DestroyEntity(entity);
            })
            .Schedule();

            UiManager.instance.destroyAll = false;
        }
    }
}
