using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;

public struct EnemyData : IComponentData
{
    public float3 location;
    public Entity entity;
    public float speed;
    public float timerInit;
}
public struct PlayerData : IComponentData
{
    public float3 location;
    public float speed;
    public int lvl;
    public int exp;
}

public struct RandomComponent : IComponentData
{
    public Unity.Mathematics.Random random;
}
public struct EnemySpawnerComponent : IComponentData
{
    public Entity EnemyPrefabs;
    public Entity XmarkPrefabs;
}

public struct XmarkSpawnerComponent : IComponentData
{
    public float timer;
    public Entity Xmark;
    public float3 location;
}


public struct EnemyTag : IComponentData
{
}

public struct XmarkTag : IComponentData
{
}
public struct Translation : IComponentData
{
    public float3 value;
}

public struct TowerData : IComponentData
{
    public Entity Prefab;
    public float Timer;
    public float TimeToNextSpawn;
    public CollisionFilter Filter;
    public float Range;
}

public struct ProjectileData : IComponentData
{
    public Entity Value;
    public float3 Position;
    public float Speed;
}

public struct DropSpawnerComponent : IComponentData
{
    public Entity DropPrefabs;
    public int SpawnCount;
    public float3 MinSpawnOffset;
    public float3 MaxSpawnOffset;
}

public struct DropItemComponent : IComponentData
{
    public float3 location;
    public float range;
    public float Speed;
    public int expAmount;
    public Entity entity;

}

public class HealthBarEntity : IComponentData
{
    public float hp;
    public float maxHp;
    public Image hpBar;

}