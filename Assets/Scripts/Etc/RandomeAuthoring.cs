using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
public class RandomeAuthoring : MonoBehaviour
{
}

public class RandomeBaker : Baker<RandomeAuthoring>
{
    public override void Bake(RandomeAuthoring authoring)
    {
        AddComponent(new RandomComponent
        {
            random = new Unity.Mathematics.Random(1)
        });
    }

}
