using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;

public class PlayerMono : MonoBehaviour
{
    public float speed = 5f;
    public float maxX = 10f;
    public float minX = -10f;
    public float maxZ = 10f;
    public float minZ = -10f;

    public SkeletonAnimation skeletonAnimation;
    private bool isIdleAnimationPlayed = false;
    private bool isRunAnimationPlayed = false;
    private bool isFacingLeft = false;
    private bool isPre = false;

    void Start()
    {

        PlayIdleAnimation();
    }

    void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical") * 1.7f;

        Vector3 inputVector = new Vector3(horizontalInput, 0f, verticalInput);
        Vector3 newPosition = transform.position + inputVector * speed * Time.deltaTime;

        float clampedX = Mathf.Clamp(newPosition.x, minX, maxX);
        float clampedZ = Mathf.Clamp(newPosition.z, minZ, maxZ);

        bool isMoving = Mathf.Abs(horizontalInput) > 0 || Mathf.Abs(verticalInput) > 0;
        // Debug.Log(horizontalInput);
        if (isMoving)
        {
            isFacingLeft = horizontalInput < 0;
            if (!isRunAnimationPlayed || isPre != isFacingLeft)
            {
                isPre = isFacingLeft;
                PlayRunAnimation(isFacingLeft);
            }
        }
        else
        {
            if (!isIdleAnimationPlayed)
            {
                PlayIdleAnimation(isFacingLeft);
            }
        }

        transform.position = new Vector3(clampedX, transform.position.y, clampedZ);
    }

    void PlayIdleAnimation(bool mirror = false)
    {
        isIdleAnimationPlayed = true;
        isRunAnimationPlayed = false;
        skeletonAnimation.AnimationState.SetAnimation(0, "idle", true);
        skeletonAnimation.Skeleton.ScaleX = mirror ? 1f : -1f;
    }

    void PlayRunAnimation(bool mirror = false)
    {
        isRunAnimationPlayed = true;
        isIdleAnimationPlayed = false;
        skeletonAnimation.AnimationState.SetAnimation(0, "run", true);
        skeletonAnimation.Skeleton.ScaleX = mirror ? 1f : -1f;
    }
}
