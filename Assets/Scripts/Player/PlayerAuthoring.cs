using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
public class PlayerDataAuthoring : MonoBehaviour
{
    public float3 location;
    public float speed;
    public int lvl;
    public int exp;
}

public class PlayerBaker : Baker<PlayerDataAuthoring>
{
    public override void Bake(PlayerDataAuthoring authoring)
    {
        AddComponent(new PlayerData
        {
            location = authoring.location,
            speed = authoring.speed,
            lvl = authoring.lvl,
            exp = authoring.exp
        });
    }

}