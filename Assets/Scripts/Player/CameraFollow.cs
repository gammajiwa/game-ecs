using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform player;
    public Vector3 offset;
    public float maxX;
    public float minX;
    public float maxY;
    public float minY;
    public float maxZ;
    public float minZ;

    void Update()
    {
        if (player != null)
        {
            float clampedX = Mathf.Clamp(player.position.x + offset.x, minX, maxX);
            float clampedY = Mathf.Clamp(player.position.y + offset.y, minY, maxY);
            float clampedZ = Mathf.Clamp(player.position.z + offset.z, minZ, maxZ);

            transform.position = new Vector3(clampedX, clampedY, clampedZ);
        }
    }
}
