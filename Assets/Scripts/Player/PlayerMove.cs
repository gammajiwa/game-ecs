using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using UnityEngine.InputSystem;
[UpdateInGroup(typeof(InitializationSystemGroup), OrderLast = true)]
public partial class PlayerMove : SystemBase
{
    // private Controls movementInput;

    private GameObject posPlayer;
    protected override void OnCreate()
    {
        // movementInput = new InputAction("Move", InputActionType.Value, "<Gamepad>/leftStick");
        // movementInput = new Controls();
    }

    protected override void OnStartRunning()
    {
        posPlayer = GameObject.FindGameObjectWithTag("Player");
    }

    protected override void OnUpdate()
    {
        float deltaTime = SystemAPI.Time.DeltaTime;
        // var inputMovement = movementInput.ControlMap.Player.ReadValue<Vector2>();
        // float3 movePlayer = new float3(inputMovement.x, 0, inputMovement.y);

        foreach (var (move, player)
            in SystemAPI.Query<RefRW<LocalTransform>, RefRW<PlayerData>>())
        {
            float3 direction = math.normalize((float3)posPlayer.transform.position - move.ValueRW.Position);
            move.ValueRW.Position += direction * SystemAPI.Time.DeltaTime * 50;
            // move.ValueRW.Position += movePlayer * player.ValueRW.speed * deltaTime;
            player.ValueRW.location = move.ValueRW.Position;
        }
    }

    // protected override void OnStartRunning()
    // {
    //     movementInput.Enable();
    // }

    // protected override void OnStopRunning()
    // {
    //     movementInput.Disable();
    // }
}
